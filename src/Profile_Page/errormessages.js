const errorMessages={
    common:{
        Mandatory:"*Field cannot be blanked*"
    }
}
export default errorMessages;