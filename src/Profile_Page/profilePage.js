import React, { Component } from 'react'
import {CInput,CLabel,CSelect,CButton, CForm, CRow,CCol,CButtonGroup, CFormGroup,CImg, CFormText,CTextarea} from "@coreui/react"
import '@coreui/coreui/dist/css/coreui.min.css'
import "./profilePage.css"
import errorMessages from "./errormessages.js"
class Profilepage extends Component {
  constructor(){
    super()
    this.state=({
      inputName:null,
      afterInputName:null,
      inputDesignation:null,
      afterinputDesignation:null,
      error:errorMessages.common.Mandatory
    })
  this.handleSubmit=this.handleSubmit.bind(this)
  }
  componentDidMount=()=>{
    const imgDiv = document.querySelector('.profile-pic-div');
    const uploadBtn = document.querySelector('#uploadBtn');
    imgDiv.addEventListener('mouseenter', function(){
      uploadBtn.style.display = "block";
    });
    
    imgDiv.addEventListener('mouseleave', function(){
      uploadBtn.style.display = "none";
    });
  }
   profile=(e)=>{
    const img = document.querySelector('#photo');
    const file = document.querySelector('#file');

file.addEventListener('change', function(){
  const choosedFile = this.files[0];
  if (choosedFile) {

      const reader = new FileReader(); 

      reader.addEventListener('load', function(){
          img.setAttribute('src', reader.result);
      });

      reader.readAsDataURL(choosedFile);
    }}
    )}
    handleSubmit=()=>{
     this.setState({
      afterInputName:this.state.inputName,
      afterinputDesignation:this.state.inputDesignation
     })
  }
  handleChange=(event)=>{
    console.log(event,"event")
    const name=event.target.name
    const value=event.target.value
    if(name=="Name"){
      this.setState({
        inputName:value,
      })
  }
    if(name=="Designation"){
      this.setState({
        inputDesignation:value,
        error:errorMessages.common.Mandatory
      })
    }
  }
  render()
  {
    const {
      inputName,
      inputDesignation,
      error
        } =this.state;
    return (
     <>
     <h1>User Profile</h1>
     <CForm className="main-div">
      <CRow className="form-group-1">
          <CCol className="Background-image"></CCol>
          <CCol className="profile-pic-div">
  <CImg src="image.jpg" id="photo" ></CImg>
  <CInput type="file" id="file" onClick={this.profile}></CInput>
  <CLabel htmlFor="file" id="uploadBtn"></CLabel>
    </CCol>
    <CCol className="Name-Set"><h3 id="Name-input">{this.state.afterInputName}</h3>
    <h5 id="Designation-input">{this.state.afterinputDesignation}</h5></CCol>
          </CRow>
      <CFormGroup className="form-group-2">
          <CCol className="col1"><span className='Mandatory'>*</span><CLabel>Name</CLabel>
            <CInput  size='sm' name='Name' onChange={this.handleChange} value={inputName}>
            </CInput>
            {!inputName ?(
            <CFormText name='Name' color='red' style={{display:"block"}} className="Error-field">{error}</CFormText>):""}
            </CCol>
          <CCol className="col2"> 
          <span className='Mandatory'>*</span>
          <CLabel>Email</CLabel>
            <CInput  size='sm'>
            </CInput>
            <CFormText  color='red' style={{display:"none"}} className="Error-field">{error}</CFormText>
            </CCol>
            {/* </CRow> */}
            {/* <CRow className="row2"> */}
              <CCol className="col3">
                <span className='Mandatory'>*</span>
              <CLabel>Date</CLabel>
            <CInput  size='sm'
             type='date'>
            </CInput>
            <CFormText  color='red' style={{display:"none"}} className="Error-field">{error}</CFormText>
            </CCol>
            <CCol className="col4">
            <span className='Mandatory'>*</span>
            <CLabel>Gender</CLabel>
            <CSelect  size='sm'>
              <option>--Select--</option>
              <option>Male</option>
              <option>Female</option>
            </CSelect>
            <CFormText  color='red' style={{display:"none"}} className="Error-field">{error}</CFormText>
            </CCol>
            <CCol className="col5">
            <span className='Mandatory'>*</span>
            <CLabel>Phone</CLabel>
            <CInput  size='sm'></CInput>
            <CFormText  color='red' style={{display:"none"}} className="Error-field">{error}</CFormText>
            </CCol>
            {/* </CRow> */}
            {/* <CRow className="row3"> */}
              <CCol className="col6">
              <span className='Mandatory'>*</span>
              <CLabel>Address</CLabel>
            <CTextarea  size='lg'></CTextarea>
            <CFormText  color='red' style={{display:"none"}} className="Error-field">{error}</CFormText>
            </CCol>
            {/* </CRow> */}
            {/* <CRow className="row4">   */}
            <CCol className="col7">
            <span className='Mandatory'>*</span>
              <CLabel>Designation</CLabel>
            <CInput  size='sm' name='Designation' value={inputDesignation}
            onChange={this.handleChange}></CInput>
            {!inputDesignation ?(
            <CFormText color='red' className="Error-field">{error}</CFormText>):""}
            </CCol>
            {/* </CRow> */}
            {/* <CRow> */}
              <CButtonGroup className="Button-group">
              <CButton color='primary'
              size='xl' onClick={this.handleSubmit}>Save</CButton>
              <CButton color='danger'
              size='xl'>Reset</CButton>
            </CButtonGroup>
      </CFormGroup>
     </CForm>
     </>
    )
  }
}
export default Profilepage;
