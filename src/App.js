import logo from './logo.svg';
import './App.css';
import Profilepage from './Profile_Page/profilePage';
function App() {
  return (
    <div className="App">
      <Profilepage />
    </div>
  );
}

export default App;
